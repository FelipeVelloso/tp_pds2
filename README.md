# TP_PDS2

Trabalho prático - Máquina de Buscas

Feito por:

Felipe Velloso Campos - 2018103550
Pedro Gabriel Amorim Soares - 2018091845


Para utilizar a função "ranking_coseno" basta definir o diretório onde os arquivos que serão comparados estão, na variável "diretorio_de_todos_arquivos".
E endereço do arquivo que será buscado na variável "diretorio_do_arquivo_a_pesquiar".