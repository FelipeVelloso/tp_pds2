#include <iostream>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <functional>
#include <math.h>
#include <experimental/filesystem>
#include <vector>
#include <string>
#include <map>
#include <list>

using namespace std;
namespace fs = experimental::filesystem;

//Função auxiliar
bool contains(const vector<string>& v, const string &s);

//Função que calcula o TF
map<string,int> busca_palava(const string& filename){
  //cout << "Lendo " << filename << "..." << endl;

  map<string, int> wordCounts;

  ifstream input;
  input.open(filename);

  string word;
  map<string, int>::iterator it;

  while (input >> word){//leia uma palavra
    transform(word.begin(), word.end(), word.begin(), ::tolower);
		word.erase(std::remove(word.begin(), word.end(), '-'), word.end());
		word.erase(std::remove(word.begin(), word.end(), ','), word.end());
		word.erase(std::remove(word.begin(), word.end(), '.'), word.end());
		word.erase(std::remove(word.begin(), word.end(), '!'), word.end());
		word.erase(std::remove(word.begin(), word.end(), '?'), word.end());
		word.erase(std::remove(word.begin(), word.end(), ';'), word.end());

    //Checa se a palavra está no map
    it = wordCounts.find(word);

    //Se a palavra não está, adicione-a
    if (it == wordCounts.end()){
      wordCounts[word] = 1;
    }
    else{
        //Se já está contida, aumente a repetição em 1
        wordCounts[word] = wordCounts[word] + 1;
    }
  }
  return wordCounts;
}

//Função que retorna quantos arquivos .txt existem no diretório
int arquivos_dir(const string dir){
  int numero_de_arquivos = 0;
  for (const auto & p : fs::directory_iterator(dir)){
      numero_de_arquivos++;
  }
  return numero_de_arquivos;
}

//Retorna os nomes dos arquivos no diretorio
vector<string> nome_arquivos(const string dir){
  vector<string> nomes;
  for (const auto & p : fs::directory_iterator(dir)){
      nomes.push_back(p.path());
  }
  return nomes;
}

//Cria um vetor com os nomes das palavras baseado no mapa do IDF
vector<string> todas_as_palavras(map<string, double> mapa_contendo_idf){
  map<string, double>:: iterator it;

  //Vetor contendo as palavras
  vector<string> palavras_apenas;

  for(it = mapa_contendo_idf.begin(); it != mapa_contendo_idf.end(); it++){
    palavras_apenas.push_back(it -> first);
  }
}

//Conta em quantos documentos diferentes cada palavra aparece
map<string,int> palavras_em_documentos(const string& path_dir){
  map<string, int> palavras_gerais;
  string word;
  string path = path_dir;
  map<string, int>::iterator it;
  list<string>::iterator it_list;

  for (const auto & p : fs::directory_iterator(path)){
    list<string> adicionado;
      ifstream input;
      input.open(p.path());
      while (input >> word){//leia uma palavra
        transform(word.begin(), word.end(), word.begin(), ::tolower);
        word.erase(std::remove(word.begin(), word.end(), '-'), word.end());
        word.erase(std::remove(word.begin(), word.end(), ','), word.end());
        word.erase(std::remove(word.begin(), word.end(), '.'), word.end());
        word.erase(std::remove(word.begin(), word.end(), '!'), word.end());
        word.erase(std::remove(word.begin(), word.end(), '?'), word.end());
        word.erase(std::remove(word.begin(), word.end(), ';'), word.end());

        //Checa se a palavra está no map
        it = palavras_gerais.find(word);

        //Se a palavra não está, adicione-a
        if (it == palavras_gerais.end()){
          palavras_gerais[word] = 1;
          adicionado.push_back(word);

        }
        else{
            it_list = find(adicionado.begin(), adicionado.end(), word);
            adicionado.push_back(word);
            if (it_list == adicionado.end()){
              //Se a palavra não foi adicionada na mesma iteração, aumente a repetição em 1
              palavras_gerais[word] = palavras_gerais[word] + 1;
          }
        }
      }
  }
  return palavras_gerais;
}

//Calcula o idf
map<string, double> calculo_idf(string dir){
  map<string, int> ocorrencias_doc = palavras_em_documentos(dir);
  int numero_de_arquivos = arquivos_dir(dir);

  map<string, double> idf;
  map<string, int>::iterator it_oc;

  for (it_oc = ocorrencias_doc.begin(); it_oc != ocorrencias_doc.end(); it_oc++){
    idf[it_oc -> first] = log((static_cast<double>(numero_de_arquivos)/it_oc -> second));
  }
  return idf;
}

//Matriz contendo o peso W = idf * tf
vector<vector<double>> matriz_pesos(string diretorio){

  //Inicializa a matriz dos pesos
  vector<vector<double>> matriz_dos_pesos;

  //Número total de arquivos
  int numero_de_documentos = arquivos_dir(diretorio);
  //Número total de palavras
  map<string,double> idf_map = calculo_idf(diretorio);

  int numero_total_de_palavras = idf_map.size();

  //Vetor contendo os nomes dos arquivos
  vector<string> nome_dos_arquivos = nome_arquivos(diretorio);

  //Matriz pesos
  for(int i = 0; i < numero_de_documentos; i++){
    //cout << "Documento: " << i << endl;
    vector<double> pesos_dj;
    string este_arquivo = nome_dos_arquivos[i];
    //Map contendo os arquivos deste documento
    map<string,int> este_map = busca_palava(este_arquivo);

    //Iterator para percorer o map
    map<string,double>:: iterator it;
    for(it = idf_map.begin(); it != idf_map.end(); it++){
      map<string,int>::iterator contem;
      contem = este_map.find(it -> first);
      if(contem != este_map.end()){
        pesos_dj.push_back(it -> second * contem -> second);
      }
      else{
        pesos_dj.push_back(0);
      }
    }
    matriz_dos_pesos.push_back(pesos_dj);
  }
  return matriz_dos_pesos;
}

//Calcula o cosine ranking
map<string, double> ranking_coseno(const string& diretorio, const string& arquivo_qi){
  //Cria a matriz dos pesos
  vector<vector<double>> matriz = matriz_pesos(diretorio);

  //Cria mapa qi, sendo qi o documento a comparar com os demais
  map<string, int> mapa_qi = busca_palava(arquivo_qi);

  //Cria o mapa dos idfs
  map<string, double> mapa_idf = calculo_idf(diretorio);


  //Cria um vetor com o peso das palavras dj no documento qi
  vector<double> vetor_qi;

  //Iteração para calular os pesos
  map<string, double>:: iterator it;

  //Cria iterator que vai percorer o map do TF
  map<string, int >::iterator it_tf;

  for(it = mapa_idf.begin(); it != mapa_idf.end(); it++){

    //Avalia se a palavra da iteração em IDF pertence ao TF
    it_tf = mapa_qi.find(it -> first);

    //Se a palavra do IDF pertencer ao TP, calcula-se o peso da mesma
    if (it_tf != mapa_qi.end()){
      vetor_qi.push_back(it -> second * it_tf -> second);
    }
    else{
      vetor_qi.push_back(0);
    }
  }

  //Inicializa o map que receberá o documento e seu valor da função cosseno
  map<string, double> mapa_cosseno;

  vector<string> palavras = todas_as_palavras(mapa_idf);
  vector<string> arquivos = nome_arquivos(diretorio);

  double numerador = 0;
  double denominador1 = 0;
  double denominador2 = 0;

  for (int i = 0; i < matriz.size(); i++){
    for (int j = 0; j < vetor_qi.size(); j++){
      numerador = numerador + (vetor_qi[j] * matriz[i][j]);
      denominador1 = denominador1 + (matriz[i][j] * matriz[i][j]);
      denominador2 = denominador2 + (vetor_qi[j] * vetor_qi[j]);
  }
    mapa_cosseno[arquivos[i]] = numerador/(sqrt(denominador1) * sqrt(denominador2));
  }
  return mapa_cosseno;
}

//Função que retorna os três arquivos mais similares
map<int, string> posicoes(map<string,double> ranking_contendo_cosseno){
  int k = 1;
  map<string, double>:: iterator it;
  map<int,string> ranking;
  while (k <= 3){
    double maior = 0;
    string nome_maior;
    for (it = ranking_contendo_cosseno.begin(); it != ranking_contendo_cosseno.end(); it++){
      if(it -> second > maior){
        maior = it -> second;
        nome_maior = it -> first;
      }
    }
    ranking_contendo_cosseno[nome_maior] = 0;
    ranking[k] = nome_maior;
    k++;
  }
  return ranking;
}


int main(){
  //Deve-se definir o diretório onde os documentos que serão buscados estão
  string diretorio_de_todos_arquivos = "/home/felipe/tp_pds2/Base/";
  //Deve-se definir o diretório onde o arquivo buscado está, o nome deste deve ser especificado
  string diretorio_do_arquivo_a_pesquiar = "/home/felipe/tp_pds2/53225";

  map<string,double> mapa_ranking = ranking_coseno(diretorio_de_todos_arquivos, diretorio_do_arquivo_a_pesquiar);

  map<string,double>:: iterator it;

  for(it = mapa_ranking.begin(); it != mapa_ranking.end(); it++){
    cout << it -> first << ": " << it -> second << endl;
  }

  map <int, string> pos = posicoes(mapa_ranking);

    map<int, string>:: iterator it_1;

    for(it_1 = pos.begin(); it_1 != pos.end(); it_1++){
      cout << it_1 -> first << ": " << it_1 -> second << endl;
    }


  return 0;

}
